package com.falcode.sportapp;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.falcode.sportapp.fragments.Fragment_Cardio;
import com.falcode.sportapp.fragments.Fragment_Drill;
import com.falcode.sportapp.fragments.Fragment_Gym;
import com.falcode.sportapp.fragments.Fragment_Nutrition;
import com.falcode.sportapp.fragments.Fragment_Transform;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Fragment_Drill fDrill;
    Fragment_Gym fGym;
    Fragment_Cardio fCardio;
    Fragment_Nutrition fNutrition;
    Fragment_Transform fTrans;
    FragmentTransaction fTransaction;
    FragmentTransaction fTransactionTwo;
    DrawerLayout drawer;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FontsOverride.setDefaultFont(this, "MONOSPACE", "roboto.ttf");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Fresco.initialize(this);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawer.openDrawer(GravityCompat.START);

        fDrill = new Fragment_Drill();
        fTransaction = getSupportFragmentManager().beginTransaction();
        fTransaction.replace(R.id.container, fDrill);
        fTransaction.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //        name_in_header.setTypeface(roboto);
//        email_in_header.setTypeface(roboto);

        fDrill = new Fragment_Drill();
        fGym = new Fragment_Gym();
        fCardio = new Fragment_Cardio();
        fNutrition = new Fragment_Nutrition();
        fTrans = new Fragment_Transform();

    }

    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();
        fTransactionTwo = getSupportFragmentManager().beginTransaction();

        if (id == R.id.nav_drill) {

            fTransactionTwo.replace(R.id.container, fDrill);
            // Handle the camera action
        } else if (id == R.id.nav_gym) {
            fTransactionTwo.replace(R.id.container, fGym);

        } else if (id == R.id.nav_cardio) {
            fTransactionTwo.replace(R.id.container, fCardio);

        } else if (id == R.id.nav_nutrition) {
            fTransactionTwo.replace(R.id.container, fNutrition);

        } else if (id == R.id.nav_transformation) {
            fTransactionTwo.replace(R.id.container, fTrans);

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
        fTransactionTwo.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
